# Industrial IoT Protocols

## 4-20mA protocol

The 4-20 mA current loop is the prevailing process control signal in many industries. It is an ideal method of transferring process information because 
current does not change as it travels from transmitter to receiver. It is also much simpler and cost effective. 
However, voltage drops and the number of process variables that need to be monitored can impact its cost and complexity.

### Components

![Components of 4-20mA protocol](/assignments/summary/assets/4-20mA.jpg)

 - **Sensor:** Measures a process variable whether temperature, humidity, flow, level or pressure.
 
 - **Transmitter:** Converts whatever the sensor is monitoring, into a current signal between 4 and 20 mA.
 
 - **Power source:** Acts as a DC source to produce the electrical signal.
 
 - **Loop:** In addition to the DC supply, a loop which refers to the actual wire connecting the sensor to the device receiving the 4-20 mA signal and then back 
 to the transmitter.
 
 - **Reciever:** A device that can recieve and interpret the current electrical signal. Since, This current signal must be translated into units that can be 
 easily understood by operators, such as the feet of liquid in a tank or the temperature in Celsius of a liquid.
 
### Features

 - Dominant standard in many industries.
 - Easy to connect and configure.
 - Less wiring and connections required. Hence, reduces initial setup costs.
 - Better for travelling across long distances as current does not degrade over long connections like Voltage.
 - Less sensitive to background electrical noise.
 - Simple to detect a fault in the system, as 4mA implies zero output.
 - Current loops can transmit only one particular process signal.
 - Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to 
 problems with ground loops if independent loops are not properly isolated.
 - These isolation requirements become exponentially more complicated as the number of loops increases.

## Modbus

MODBUS is a commonly used industrial communications protocol. It allows the exchange of data between PLCs and computers.
Used commonly in IoT as a local interface to manage devices.

 - The device requesting information is called the **master** and the device supplying information is called the **slave**.
 - In a standard MODBUS configuration, there is one master and 247 slaves (each with a unique slave address ranging from 1 to 247).
 - Communication between a master and a slave is established in a frame indicating a function code.	
 - The function code helps identify the action to perform, such as read a discrete input, read a first-in, first-out queue, or perform a diagnostic function.
 - The slave then responds based on the function code recieved.
 - Can be used over two interfaces:
	 - RS485 (Modbus RTU)
		 - RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
		 - RS485 is not directly compatible, we must use the correct type of interface, or the signals won't go through. 
		 Mainly done through an easy to use RS485-USB.
	 - Ethernet (Modbus TCP/IP)

## OPCUA communication protocol

OPC Unified Architecture (OPC UA) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation.
 
### Features

 - The OPC UA supports two protocols namely, **TCP** which is a binary protocol and **HTTP** which is for web services (fully capable of moving data over the internet).
 - The UA APIs are available in several programming languages including C, C++, Java, Python, JavaScript and .NET. It is also platform-independent.
 - Security - ensures the authentication of Client and Servers, the authentication of users and the integrity of their communication.
 - Efficient Event Management - uses a highly configurable mechanism for providing alarms and event notifications to interested clients.
 - The architecture of a UA (Unified Architecture) application, independent of whether it is the server or client part, is structured into levels.
 - Efficient Integration with standard industry-specific data models.

| Client | Server |
| --------------- | --------------- |
| ![OPCUA](/assignments/summary/assets/opcua-client.png) | ![OPCUA](/assignments/summary/assets/opcua-server.png) |

# Cloud Protocols

## Message Queuing Telemetry Transport (MQTT)

![MQTT working](/assignments/summary/assets/mqtt.png)

 - Designed for constrained devices with low bandwidth.
 - A minimal publish and subscribe system to publish and receive messages as a client.
 - Allows to send commands to control outputs, read and publish data from sensor nodes.
 - Communication between several devices can be established simultaneously.
 
### A few terminologies

 - **MQTT Client** as a publisher sends a message to the **MQTT broker** whose work is to distribute the message accordingly to all other MQTT clients 
 subscribed to the topic on which publisher publishes the message.
 - **Topics** are a way to register interest for incoming messages or to specify where to publish the message. (represented by strings separated by a '/')

## Hyper Text Transfer Protocol (HTTP)

 - It is a request-response protocol.
 - **Client** sends an HTTP request.
 - **Server** sends back an HTTP response.

### HTTP Request

Delving into the back-end, a request has three fundamental parts:

 - Request line
 - HTTP headers
 - Message body

#### Types of methods

 - **GET:** Retrieve resource from server.
 - **POST:** Create resource on server.
 - **PUT/PATCH:** Update resource on server.
 - **DELETE:** Delete resource from server.
 
### HTTP Response

Delving into the back-end, a response has three fundamental parts:

 - Status line
 - HTTP headers
 - Message body

![HTTP status codes](/assignments/summary/assets/http-status-codes.jpg)

### Examples: Request & Response headers

![Request and Response headers](/assignments/summary/assets/response-and-request-headers.jpg)

 